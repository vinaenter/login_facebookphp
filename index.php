<?php
session_start(); 
?>
<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml"> 
	<head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

		<script type="text/javascript" src="js/script.js"> </script>
	</head>
<body>
	
	  <?php if ($_SESSION['FBID']): ?>      <!--  After user login  -->
<div class="container">
<div class="hero-unit">
  <h1>Hello <?php echo $_SESSION['USERNAME']; ?></h1>
  <p>Welcome to "facebook login" tutorial</p>
  </div>
<div class="span4">
 <ul class="nav nav-list">
<li class="nav-header">Image</li>
	<li><img src="https://graph.facebook.com/<?php echo $_SESSION['FBID']; ?>/picture"></li>
<li class="nav-header">Facebook ID</li>
<li><?php echo  $_SESSION['FBID']; ?></li>
<li class="nav-header">Facebook fullname</li>
<li><?php echo $_SESSION['FULLNAME']; ?></li>
<li class="nav-header">Facebook Email</li>
<li><?php echo $_SESSION['EMAIL']; ?></li>
<div><a href="logout.php">Logout</a></div>
</ul></div></div>

<?php endif ?>

  <div class="login-box">
  <div class="lb-header">
    <a href="#" class="active" id="login-box-link">Login</a>
  </div>
  <div class="social-login">
    <a href="fbconfig.php">
        <i class="fa fa-facebook fa-lg"></i>
        Login in with facebook
      </a>
    <a href="#">
        <i class="fa fa-google-plus fa-lg"></i>
        log in with Google
      </a>
  </div>
  
 
</div>
</body>
</html>

