<?php
session_start();
// added in v4.0.0
require_once 'ConnectDBUtil.php';
require_once 'autoload.php';
require_once 'functions.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
// init app with app id and secret
FacebookSession::setDefaultApplication( '1619645228306646','8c7d3aac15452698a2c4e6226374345a' );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper('https://loginit.com/login/fbconfig.php' );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session
if ( isset( $session ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
	$fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
	$femail = $graphObject->getProperty('email');    // To Get Facebook email ID
	/* ---- Session Variables -----*/
	$_SESSION['FBID'] = $fbid;           
	$_SESSION['FULLNAME'] = $fbfullname;
	$_SESSION['EMAIL'] =  $femail;
	/* ---- header location after session ----*/
	$check = $mysqli->query("select * from users where fuid='$fbid'");
	$check = mysqli_num_rows($check);
	$result = false;
	if (empty($check)) { 
		// if new user . Insert a new record		
		$query = "INSERT INTO users (fuid,ffname,femail) VALUES ('$fbid','$fbfullname','$femail')";
		$result = $mysqli->query($query);	
	} else {   
		// If Returned user . update the user record		
		$query = "UPDATE users SET ffname='$fbfullname', femail='$femail' where fuid='$fuid'";
		$result = $mysqli->query($query);
	}
	
	if($result) {
		header("location:https://itphutran.com");
		die();
	} else {
		header("location:/index.php?err=ErrLogin");
		die();
	}
} else {
  $loginUrl = $helper->getLoginUrl();
  header("Location: ".$loginUrl);
}
?>